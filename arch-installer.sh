#!/usr/bin/env bash

# =================================================== ::
#
# Author       ::sum4n
# Source       ::https://gitlab.com/sum4n/bin
# Maintainer   ::to_suman@outlook.com
# Project      ::arch linux installation script
#
# =================================================== ::


# true / false
TRUE=0
FALSE=1

# return codes
SUCCESS=0
FAILURE=1

# verbose mode - default: quiet
verbose='/dev/null'

# colors
WHITE="$(tput setaf 7)"
# WHITEB="$(tput bold ; tput setaf 7)"
# BLUE="$(tput setaf 4)"
BLUEB="$(tput bold ; tput setaf 4)"
CYAN="$(tput setaf 6)"
CYANB="$(tput bold ; tput setaf 6)"
# GREEN="$(tput setaf 2)"
GREENB="$(tput bold ; tput setaf 2)"
RED="$(tput setaf 1)"
# REDB="$(tput bold; tput setaf 1)"
YELLOW="$(tput setaf 3)"
YELLOWB="$(tput bold ; tput setaf 3)"
NC="$(tput sgr0)"

# chosen locale
LOCALE=''

# set locale
SET_LOCALE='1'

# list locales
LIST_LOCALE='2'

# chosen keymap
KEYMAP=''

# set keymap
SET_KEYMAP='1'

# list keymaps
LIST_KEYMAP='2'

# network interfaces
NET_IFS=''

# chosen network interface
NET_IF=''

# network configuration mode
NET_CONF_MODE=''

# network configuration modes
NET_CONF_AUTO='1'
NET_CONF_WLAN='2'
NET_CONF_MANUAL='3'
NET_CONF_SKIP='4'

# hostname
HOST_NAME=''

# host ipv4 address
HOST_IPV4=''

# gateway ipv4 address
GATEWAY=''

# subnet mask
SUBNETMASK=''

# broadcast address
BROADCAST=''

# nameserver address
NAMESERVER=''

# DualBoot flag
DUALBOOT=''

# avalable hard drive
HD_DEVS=''

# chosen hard drive device
HD_DEV=''

# Partitions
PARTITIONS=''

# partition label: gpt or dos
PART_LABEL=''

# boot partition
BOOT_PART=''

# root partition
ROOT_PART=''

# home partition
HOME_PART=''

# crypted root
CRYPT_ROOT='r00t'

# swap partition
SWAP_PART=''

# boot fs type - default: ext4
BOOT_FS_TYPE=''

# root fs type - default: ext4
ROOT_FS_TYPE=''

# home fs type - default: ext4
HOME_FS_TYPE=''

# chroot directory / arch linux installation
CHROOT='/mnt'

# normal system user
NORMAL_USER=''

# wlan ssid
WLAN_SSID=''

# wlan passphrase
WLAN_PASSPHRASE=''

# check boot mode
BOOT_MODE=''

# Exit on CTRL + c
ctrl_c() {
  err "Keyboard Interrupt detected, leaving.."
  exit $FAILURE
}

trap ctrl_c 2


# check exit status
check()
{
  es=$1
  func="$2"
  info="$3"

  if [ "$es" -ne 0 ]
  then
    echo
    warn "Something went wrong with $func. $info."
    sleep 5
  fi
}


# print formatted output
printl()
{
  fmt="${1}"

  shift
  printf "%s$fmt%s" "$WHITE" "$@" "$NC"

  return $SUCCESS
}


# print warning
warn()
{
  printf "%s[!] WARNING: %s%s\n" "$YELLOW" "$@" "$NC"

  return $SUCCESS
}

# -- [ print question ] -- #
ask()
{
  que="${1}"

  shift
  printf "%s[?] $que%s" "$CYANB" "$@" "$NC"

  return $SUCCESS
}

# -- [ print already in system ] -- #
already()
{
  printf "%s[~] %s%s\n" "$GREENB" "$@" "$NC"

  return $SUCCESS
}

###  [ print error and return failure ]  ###
err()
{
  printf "%s[-] ERROR: %s%s\n" "$RED" "$@" "$NC"

  return $FAILURE
}

# --- [ personalized banner ] --- #
banner()
{
  columns="$(tput cols)"
  str="---- [ sedo-desktop installer // Author :: Suman Kumar Das // Source :: https://gitlab.com/sum4n/ ] ----"

  printf "${BLUEB}%*s${NC}\n" "${COLUMNS:-$(tput cols)}" | tr ' ' '-'

  echo "$str" |
  while IFS= read -r line
  do
    printf "%s%*s\n%s" "$CYANB" $(( (${#line} + columns) / 2)) \
      "$line" "$NC"
  done

  printf "${BLUEB}%*s${NC}\n\n\n" "${COLUMNS:-$(tput cols)}" | tr ' ' '-'

  return $SUCCESS
}


# check boot mode
check_boot_mode()
{
  efivars=$(ls /sys/firmware/efi/efivars > /dev/null 2>&1; echo $?)
  if [ "$efivars" -eq "0" ]
  then
     BOOT_MODE="uefi"
  fi

  return $SUCCESS
}

# sleep and clear
sleep_clear()
{
  sleep "$1"
  clear

  return $SUCCESS
}

# confirm user inputted yYnN
confirm()
{
  header="$1"
  ask="$2"

  while true
  do
    title "$header"
    printl "$ask"
    read -r input
    case $input in
      y|Y|yes|YES|Yes) return $TRUE ;;
      n|N|no|NO|No) return $FALSE ;;
      *) clear ; continue ;;
    esac
  done

  return $SUCCESS
}

install_keyrings()
{
  pacman -S archlinux-keyring --noconfirm
}


# print menu title
title()
{
  banner
  printf "${CYAN}>> %s${NC}\n\n\n" "${@}"

  return "${SUCCESS}"
}


# check for environment issues
check_env()
{
  if [ -f '/var/lib/pacman/db.lck' ]
  then
    err 'pacman locked - Please remove /var/lib/pacman/db.lck'
  fi
}

#--[ Enable ParallelDownloads ]--#
paralleldownloads()
{
  printl "[?] Check the paralleldownloads are enable\n" && sleep 1
  if grep -q "#ParallelDownloads " /etc/pacman.conf; then
    warn "ParallelDownloads not enabled" && sleep 1
    sed -i "s/^#ParallelDownloads = 5/ParallelDownloads = 8/" /etc/pacman.conf
    printl "(: ParallelDownloads enabled now\n" && sleep 1
  fi
}


# check user id
check_uid()
{
  if [ "$(id -u)" != '0' ]
  then
    err 'You must be root to run the Arch installer!'
  fi

  return $SUCCESS
}


# ask for output mode
ask_output_mode()
{
  title 'Environment > Output Mode'
  printl '[+] Available output modes:'
  printf "\n
  1. Quiet (default)
  2. Verbose (output of system commands: mkfs, pacman, etc.)\n\n"
  printl "[?] Make a choice: "
  read -r output_opt
  if [ "$output_opt" = 2 ]
  then
    verbose='/dev/stdout'
  fi

  return $SUCCESS
}


# ask for locale to use
ask_locale()
{
  while [ "$locale_opt" != "$SET_LOCALE" ] && \
    [ "$locale_opt" != "$LIST_LOCALE" ]
  do
    title 'Environment > Locale Setup'
    printl '[+] Available locale options:'
    printf "\n
  1. Set a locale
  2. List available locales\n\n"
    printl "[?] Make a choice: "
    read -r locale_opt
    if [ "$locale_opt" = "$SET_LOCALE" ]
    then
      break
    elif [ "$locale_opt" = "$LIST_LOCALE" ]
    then
      less /etc/locale.gen
      echo
    else
      clear
      continue
    fi
    clear
  done

  clear

  return $SUCCESS
}


# set locale to use
set_locale()
{
  title 'Environment > Locale Setup'
  printl '[?] Set locale [en_US.UTF-8]: '
  read -r LOCALE

  # default locale
  if [ -z "$LOCALE" ]
  then
    echo
    warn 'Setting default locale: en_US.UTF-8'
    sleep 1
    LOCALE='en_US.UTF-8'
  fi
  localectl set-locale "LANG=$LOCALE"
  check $? 'setting locale'

  return $SUCCESS
}


# ask for keymap to use
ask_keymap()
{
  while [ "$keymap_opt" != "$SET_KEYMAP" ] && \
    [ "$keymap_opt" != "$LIST_KEYMAP" ]
  do
    title 'Environment > Keymap Setup'
    printl '[+] Available keymap options:'
    printf "\n
  1. Set a keymap
  2. List available keymaps\n\n"
    printl '[?] Make a choice: '
    read -r keymap_opt

    if [ "$keymap_opt" = "$SET_KEYMAP" ]
    then
      break
    elif [ "$keymap_opt" = "$LIST_KEYMAP" ]
    then
      localectl list-keymaps
      echo
    else
      clear
      continue
    fi
    clear
  done

  clear

  return $SUCCESS
}


# set keymap to use
set_keymap()
{
  title 'Environment > Keymap Setup'
  printl '[?] Set keymap [us]: '
  read -r KEYMAP

  # default keymap
  if [ -z "$KEYMAP" ]
  then
    echo
    warn 'Setting default keymap: us'
    sleep 1
    KEYMAP='us'
  fi
  localectl set-keymap --no-convert "$KEYMAP"
  loadkeys "$KEYMAP" > $verbose 2>&1
  check $? 'setting keymap'

  return $SUCCESS
}


# # enable multilib in pacman.conf if x86_64 present
# enable_pacman_multilib()
# {
#   path="$1"
#
#   if [ "$path" = 'chroot' ]
#   then
#     path="$CHROOT"
#   else
#     path=""
#   fi
#
#   title 'Pacman Setup > Multilib'
#
#   if [ "$(uname -m)" = "x86_64" ]
#   then
#     printl '[+] Enabling multilib support'
#     printf "\n\n"
#     if grep -q "#\[multilib\]" "$path/etc/pacman.conf"
#     then
#       # it exists but commented
#       sed -i '/\[multilib\]/{ s/^#//; n; s/^#//; }' "$path/etc/pacman.conf"
#     elif ! grep -q "\[multilib\]" "$path/etc/pacman.conf"
#     then
#       # it does not exist at all
#       printf "[multilib]\nInclude = /etc/pacman.d/mirrorlist\n" \
#         >> "$path/etc/pacman.conf"
#     fi
#   fi
#
#   return $SUCCESS
# }


# update pacman package database
update_pkg_database()
{
  title 'Pacman Setup > Package Database'

  printl '[+] Updating pacman database'
  printf "\n\n"

  pacman -Syy --noconfirm > $verbose 2>&1

  return $SUCCESS
}


# update pacman.conf and database
update_pacman()
{
  # enable_pacman_multilib "chroot"
  sleep_clear 1

  update_pkg_database
  sleep 1

  return $SUCCESS
}


# ask user for hostname
ask_hostname()
{
  while [ -z "$HOST_NAME" ]
  do
    title 'Network Setup > Hostname'
    printl '[?] Set your hostname: '
    read -r HOST_NAME
  done

  return $SUCCESS
}

# get available network interfaces
get_net_ifs()
{
  NET_IFS="$(ip -o link show | awk -F': ' '{print $2}' |grep -v 'lo')"

  return $SUCCESS
}


# ask user for network interface
ask_net_if()
{
  while true
  do
    title 'Network Setup > Network Interface'
    printl '[+] Available network interfaces:'
    printf "\n\n"
    for i in $NET_IFS
    do
      echo "    > $i"
    done
    echo
    printl '[?] Please choose a network interface: '
    read -r NET_IF
    if echo "$NET_IFS" | grep "\<$NET_IF\>" > /dev/null
    then
      clear
      break
    fi
    clear
  done

  return $SUCCESS
}


# ask for networking configuration mode
ask_net_conf_mode()
{
  while [ "$NET_CONF_MODE" != "$NET_CONF_AUTO" ] && \
    [ "$NET_CONF_MODE" != "$NET_CONF_WLAN" ] && \
    [ "$NET_CONF_MODE" != "$NET_CONF_MANUAL" ] && \
    [ "$NET_CONF_MODE" != "$NET_CONF_SKIP" ]
  do
    title 'Network Setup > Network Interface'
    printl '[+] Network interface configuration:'
    printf "\n
  1. Auto DHCP (use this for auto connect via dhcp on selected interface)
  2. WiFi WPA Setup (use if you need to connect to a wlan before)
  3. Manual (use this if you are 1337)
  4. Skip (use this if you are already connected)\n\n"
    printl "[?] Please choose a mode: "
    read -r NET_CONF_MODE
    clear
  done

  return $SUCCESS
}


# ask for network addresses
ask_net_addr()
{
  while [ "$HOST_IPV4" = "" ] || \
    [ "$GATEWAY" = "" ] || [ "$SUBNETMASK" = "" ] || \
    [ "$BROADCAST" = "" ] || [ "$NAMESERVER" = "" ]
  do
    title 'Network Setup > Network Configuration (manual)'
    printl "[+] Configuring network interface $NET_IF via USER: "
    printf "\n
  > Host ipv4
  > Gateway ipv4
  > Subnetmask
  > Broadcast
  > Nameserver
    \n"
    printl '[?] Host IPv4: '
    read -r HOST_IPV4
    printl '[?] Gateway IPv4: '
    read -r GATEWAY
    printl '[?] Subnetmask: '
    read -r SUBNETMASK
    printl '[?] Broadcast: '
    read -r BROADCAST
    printl '[?] Nameserver: '
    read -r NAMESERVER
    clear
  done

  return $SUCCESS
}


# manual network interface configuration
net_conf_manual()
{
  title 'Network Setup > Network Configuration (manual)'
  printl "[+] Configuring network interface '$NET_IF' manually: "
  printf "\n\n"

  ip addr flush dev "$NET_IF"
  ip link set "$NET_IF" up
  ip addr add "$HOST_IPV4/$SUBNETMASK" broadcast "$BROADCAST" dev "$NET_IF"
  ip route add default via "$GATEWAY"
  echo "nameserver $NAMESERVER" > /etc/resolv.conf

  return $SUCCESS
}


# auto (dhcp) network interface configuration
net_conf_auto()
{
  opts='-h noleak -i noleak -v ,noleak -I noleak -t 10'

  title 'Network Setup > Network Configuration (auto)'
  printl "[+] Configuring network interface '$NET_IF' via DHCP: "
  printf "\n\n"

  dhcpcd "$opts" -i "$NET_IF" > $verbose 2>&1

  sleep 10

  return $SUCCESS
}


# ask for wlan data (ssid, wpa passphrase, etc.)
ask_wlan_data()
{
  while [ "$WLAN_SSID" = "" ] || [ "$WLAN_PASSPHRASE" = "" ]
  do
    title 'Network Setup > Network Configuration (WiFi)'
    printl "[+] Configuring network interface $NET_IF via W-LAN + DHCP: "
    printf "\n
  > W-LAN SSID
  > WPA Passphrase (will not echo)
    \n"
    printl "[?] W-LAN SSID: "
    read -r WLAN_SSID
    printl "[?] WPA Passphrase: "
    read -rs WLAN_PASSPHRASE
    clear
  done

  return $SUCCESS
}


##----[ wifi and auto dhcp network interface configuration ]----##
net_conf_wlan()
{
  wpasup="$(mktemp)"
  dhcp_opts='-h noleak -i noleak -v ,noleak -I noleak -t 10'

  title 'Network Setup > Network Configuration (WiFi)'
  printl "[+] Configuring network interface $NET_IF via W-LAN + DHCP: "
  printf "\n\n"

  wpa_passphrase "$WLAN_SSID" "$WLAN_PASSPHRASE" > "$wpasup"
  wpa_supplicant -B -c "$wpasup" -i "$NET_IF" > $verbose 2>&1

  warn 'We need to wait a bit for wpa_supplicant and dhcpcd'

  sleep 10

  dhcpcd "$dhcp_opts" -i "$NET_IF" > $verbose 2>&1

  sleep 10

  return $SUCCESS
}


##----[ check for internet connection ]----##
check_inet_conn()
{
  title 'Network Setup > Connection Check'
  printl '[+] Checking for Internet connection...'

  if ! curl -s http://www.google.com/ > /dev/null 2>&1
  then
    err 'No Internet connection! Check your network (settings).'
    exit $FAILURE
  fi

}


##----[ ask user for dualboot install ]----##
ask_dualboot()
{
  while [ "$DUALBOOT" = '' ]
  do
    if confirm 'Hard Drive Setup > DualBoot' '[?] Install Arch Linux with Windows/Other OS [y/n]: '
    then
      DUALBOOT=$TRUE
    else
      DUALBOOT=$FALSE
    fi
  done
  return $SUCCESS
}


##----[  get available hard disks ]----##
get_hd_devs()
{
  HD_DEVS="$(lsblk | grep disk | awk '{print $1}')"

  return $SUCCESS
}


##----[ ask user for device to format and setup ]----##
ask_hd_dev()
{
  while true
  do
    title 'Hard Drive Setup'

    printl '[+] Available hard drives for installation:'
    printf "\n\n"

    for i in $HD_DEVS
    do
      echo "    > ${i}"
    done
    echo
    printl '[?] Please choose a device: '
    read -r HD_DEV
    if echo "$HD_DEVS" | grep "\<$HD_DEV\>" > /dev/null
    then
      HD_DEV="/dev/$HD_DEV"
      clear
      break
    fi
    clear
  done


  return $SUCCESS
}

#---[ get available partitions on hard drive ]---#
get_partitions()
{
  PARTITIONS=$(fdisk -l "${HD_DEV}" -o device,size,type | \
    grep "${HD_DEV}[[:alnum:]]" |awk '{print $1;}')

  return $SUCCESS
}


#---[ ask user to create partitions using cfdisk ]---#
ask_cfdisk()
{
  if confirm 'Hard Drive Setup > Partitions' '[?] Create partitions with cfdisk (root and boot, optional swap) [y/n]: '
  then
    clear
    zero_part
  else
    echo
    warn 'No partitions chosed? Make sure you have them already configured.'
    get_partitions
  fi

  return $SUCCESS
}


#---[ zero out partition if needed/chosen ]---#
zero_part()
{
  if confirm 'Hard Drive Setup' '[?] Start with an in-memory zeroed partition table [y/n]: '
  then
    cfdisk -z "$HD_DEV"
    sync
  else
    cfdisk "$HD_DEV"
    sync
  fi
  get_partitions
  if [ ${#PARTITIONS[@]} -eq 0 ]; then
    err 'You have not created partitions on your disk, make sure to write your changes before quiting cfdisk. Trying again...'
    zero_part
  fi
  if [ "$BOOT_MODE" = 'uefi' ] && ! fdisk -l "$HD_DEV" -o type | grep -i 'EFI' ; then
    err 'You are booting in UEFI mode but not EFI partition was created, make sure you select the "EFI System" type for your EFI partition.'
    zero_part
  fi
  return $SUCCESS
}


#---[ get partition label ]---#
get_partition_label()
{
  PART_LABEL="$(fdisk -l "$HD_DEV" |grep "Disklabel" | awk '{print $3;}')"

  return $SUCCESS
}


# get partitions
ask_partitions()
{
  while [ "$BOOT_PART" = '' ] || \
    [ "$ROOT_PART" = '' ] || \
    [ "$BOOT_FS_TYPE" = '' ] || \
    [ "$ROOT_FS_TYPE" = '' ]
  do
    title 'Hard Drive Setup > Partitions'
    printl '[+] Created partitions:'
    printf "\n\n"

    fdisk -l "${HD_DEV}" -o device,size,type |grep "${HD_DEV}[[:alnum:]]"

    echo

    if [ "$BOOT_MODE" = 'uefi' ]  && [ "$PART_LABEL" = 'gpt' ]
    then
      while [ -z "$BOOT_PART" ]; do
        printl "[?] EFI System partition (${HD_DEV}X): "
        read -r BOOT_PART
        until [[ "$PARTITIONS" =~ $BOOT_PART ]]; do
          printl "[?] Your partition $BOOT_PART is not in the partitions list.\n"
          printl "[?] EFI System partition (${HD_DEV}X): "
          read -r BOOT_PART
        done
      done
      BOOT_FS_TYPE="fat32"
    else
      while [ -z "$BOOT_PART" ]; do
        printl "[?] Boot partition (${HD_DEV}X): "
        read -r BOOT_PART
        until [[ "$PARTITIONS" =~ $BOOT_PART ]]; do
          printl "[?] Your partition $BOOT_PART is not in the partitions list.\n"
          printl "[?] Boot partition (${HD_DEV}X): "
          read -r BOOT_PART
        done
      done
      printl '[?] Choose a filesystem to use in your boot partition (ext2, ext3, ext4)? (default: ext4): '
      read -r BOOT_FS_TYPE
      if [ -z "$BOOT_FS_TYPE" ]; then
        BOOT_FS_TYPE="ext4"
      fi
    fi
    while [ -z "$ROOT_PART" ]; do
      printl "[?] Root partition (${HD_DEV}X): "
      read -r ROOT_PART
      until [[ "$PARTITIONS" =~ $ROOT_PART ]]; do
          printl "[?] Your partition $ROOT_PART is not in the partitions list.\n"
          printl "[?] Root partition (${HD_DEV}X): "
          read -r ROOT_PART
      done
    done
    warn "If you selected (( btrfs )) file system for (( root )) partition" && sleep 2
    warn "Then if you've created a seperate (( home )) partition earlier." && sleep 2
    warn "It's not going to work, at least in this script" && sleep 2
    warn "So in the case of seperate (( home )) partition" && sleep 3
    warn "Select one of these 3 options (( ext4, ext3, or ext2 )) for (( root )) file system" && sleep 2
    printl '[?] Choose a filesystem to use in your root partition (ext2, ext3, ext4, btrfs)? (default: ext4): '
    read -r ROOT_FS_TYPE
    if [ -z "$ROOT_FS_TYPE" ]; then
      ROOT_FS_TYPE="ext4"
    fi
    printl "[?] Swap partition (${HD_DEV}X - empty for none): "
    read -r SWAP_PART
    if [ -n "$SWAP_PART" ]; then
        until [[ "$PARTITIONS" =~ $SWAP_PART ]]; do
          printl "[?] Your partition $SWAP_PART is not in the partitions list.\n"
          printl "[?] Swap partition (${HD_DEV}X): "
          read -r SWAP_PART
        done
    fi

    if [ "$SWAP_PART" = '' ]
    then
      SWAP_PART='none'
    fi

    printl "[?] Home partition (${HD_DEV}X - empty for none): "
    read -r HOME_PART
    if [ -n "$HOME_PART" ] && [ "$ROOT_FS_TYPE" != "btrfs" ]; then
      until [[ "$PARTITIONS" =~ $HOME_PART ]]; do
        printl "[?] Your partition $HOME_PART is not in the file system.\n"
        printl "[?] Home partition (${HD_DEV}X): "
        read -r HOME_PART
      done
    fi

    if [ -n "$HOME_PART" ]; then
      printl '[?] Choose a filesystem to use in your home partition (ext2, ext3, ext4)? (default: ext4): '
      read -r HOME_FS_TYPE
      if [ -z "$HOME_FS_TYPE" ]; then
        HOME_FS_TYPE="ext4"
      fi
    fi

    if [ "$HOME_PART" = '' ]
    then
      HOME_PART='none'
    fi

    clear
  done

  return $SUCCESS
}


# print partitions and ask for confirmation
print_partitions()
{
  i=""

  while true
  do
    title 'Hard Drive Setup > Partitions'
    printl '[+] Current Partition table'
    printf "\n
  > /boot   : %s (%s)
  > /       : %s (%s)
  > /home   : %s (%s)
  > swap    : %s (swap)
  \n" "$BOOT_PART" "$BOOT_FS_TYPE" \
      "$ROOT_PART" "$ROOT_FS_TYPE" \
      "$HOME_PART" "$HOME_FS_TYPE" \
      "$SWAP_PART"
    printl '[?] Partition table correct [y/n]: '
    read -r i
    if [ "$i" = 'y' ] || [ "$i" = 'Y' ]
    then
      clear
      break
    elif [ "$i" = 'n' ] || [ "$i" = 'N' ]
    then
      echo
      err 'Hard Drive Setup aborted.'
      exit $FAILURE
    else
      clear
      continue
    fi
    clear
  done

  return $SUCCESS
}


# ask user and get confirmation for formatting
ask_formatting()
{
  if confirm 'Hard Drive Setup > Partition Formatting' '[?] Formatting partitions. Are you sure? No crying afterwards? [y/n]: '
  then
    return $SUCCESS
  else
    echo
    err 'Seriously? No formatting no fun! Please format to continue or CTRL + c to cancel...'
    ask_formatting
  fi

}


# create swap partition
make_swap_partition()
{
  title 'Hard Drive Setup > Partition Creation (swap)'

  printl '[+] Creating SWAP partition'
  printf "\n\n"
  mkswap "$SWAP_PART" > $verbose 2>&1 || { err 'Could not create filesystem'; exit $FAILURE; }

}


# make and format root partition
make_root_partition()
{
  title 'Hard Drive Setup > Partition Creation (root)'
  printl '[+] Creating ROOT partition'
  printf "\n\n"
  if [ "$ROOT_FS_TYPE" = 'btrfs' ]
  then
    mkfs."$ROOT_FS_TYPE" -f "$ROOT_PART" > $verbose 2>&1 ||
      { err 'Could not create filesystem'; exit $FAILURE; }
  else
    mkfs."$ROOT_FS_TYPE" -F "$ROOT_PART" > $verbose 2>&1 ||
      { err 'Could not create filesystem'; exit $FAILURE; }
  fi
  sleep_clear 1

  return $SUCCESS
}

#---[ make home partition ]---#
make_home_partition()
{
  title 'Hard Drive Setup > Partition Creation (home)'
  printl '[+] Creating HOME partition'
  printf "\n\n"
  mkfs."$HOME_FS_TYPE" -F "$HOME_PART" > $verbose 2>&1 ||
    { err 'Could not create filesystem'; exit $FAILURE; }
  sleep_clear 1

}


# make and format boot partition
make_boot_partition()
{
  if [ "$BOOT_MODE" = 'uefi' ] && [ "$PART_LABEL" = 'gpt' ] && [ $DUALBOOT = $TRUE ]
  then
    return $SUCCESS
  fi

  title 'Hard Drive Setup > Partition Creation (boot)'

  printl '[+] Creating BOOT partition'
  printf "\n\n"
  if [ "$BOOT_MODE" = 'uefi' ] && [ "$PART_LABEL" = 'gpt' ]
  then
    mkfs.fat -F32 "$BOOT_PART" > $verbose 2>&1 ||
      { err 'Could not create filesystem'; exit $FAILURE; }
    sleep 2
  else
    mkfs."$BOOT_FS_TYPE" -F "$BOOT_PART" > $verbose 2>&1 ||
      { err 'Could not create filesystem'; exit $FAILURE; }
    sleep 2
  fi

  return $SUCCESS
}


# make and format partitions
make_partitions()
{
  make_boot_partition
  sleep_clear 1

  make_root_partition
  sleep_clear 1

  [ "$HOME_PART" != "none" ] && { make_home_partition; sleep_clear 1; }

  [ "$SWAP_PART" != "none" ] && { make_swap_partition; sleep_clear 1; }

  return $SUCCESS
}


# mount filesystems
mount_filesystems()
{
  title 'Hard Drive Setup > Mount'

  printl '[+] Mounting filesystems'
  printf "\n\n"

  # ROOT
  if ! mount "$ROOT_PART" $CHROOT; then
    err "Error mounting root filesystem, leaving."
    exit $FAILURE
  fi

  # HOME
  if [ "$HOME_PART" != "none" ]; then
    mkdir "$CHROOT/home" > $verbose 2>&1
    if ! mount "$HOME_PART" "$CHROOT/home"; then
      err "Error mounting home partition, leaving."
      exit $FAILURE
    fi
  fi

  # BOOT
  mkdir -p "$CHROOT/boot" > $verbose 2>&1
  if ! mount "$BOOT_PART" "$CHROOT/boot"; then
    err "Error mounting boot partition, leaving."
    exit $FAILURE
  fi

  # SWAP
  if [ "$SWAP_PART" != "none" ]
  then
    swapon "$SWAP_PART" > $verbose 2>&1
  fi

  return $SUCCESS
}


# unmount filesystems
umount_filesystems()
{
  routine="$1"

  if [ "$routine" = 'harddrive' ]
  then
    title 'Hard Drive Setup > Unmount'

    printl '[+] Unmounting filesystems'
    printf "\n\n"

    umount -Rf /mnt > /dev/null 2>&1; \
    umount -Rf "$HD_DEV"{1..128} > /dev/null 2>&1 # gpt max - 128
  else
    title 'Game Over'

    printl '[+] Unmounting filesystems'
    printf "\n\n"

    umount -Rf $CHROOT > /dev/null 2>&1
    cryptsetup luksClose "$CRYPT_ROOT" > /dev/null 2>&1
    swapoff "$SWAP_PART" > /dev/null 2>&1
  fi

  return $SUCCESS
}


# check for necessary space
check_space()
{
  avail_space=$(df -m | grep "$ROOT_PART" | awk '{print $4}')

  if [ "$avail_space" -le 40960 ]
  then
    warn 'Arch Linux requires at least 40 GB of free space to install!'
  fi

  return $SUCCESS
}


# install ArchLinux base and base-devel packages
install_base_packages()
{
  title 'Base System Setup > ArchLinux Packages'

  printl '[+] Installing ArchLinux base packages'
  printf "\n\n"
  warn 'This can take a while, please wait...'
  printf "\n"

  pacstrap $CHROOT base base-devel btrfs-progs linux-lts linux-firmware \
    linux-lts-docs linux-lts-headers > $verbose 2>&1
  chroot $CHROOT pacman -Syy --noconfirm --overwrite='*' > $verbose 2>&1

  return $SUCCESS
}


# setup /etc/resolv.conf
setup_resolvconf()
{
  title 'Base System Setup > Etc'

  printl '[+] Setting up /etc/resolv.conf'
  printf "\n\n"

  mkdir -p "$CHROOT/etc/" > $verbose 2>&1
  cp -L /etc/resolv.conf "$CHROOT/etc/resolv.conf" > $verbose 2>&1

  return $SUCCESS
}


# setup fstab
setup_fstab()
{
  title 'Base System Setup > Etc'

  printl '[+] Setting up /etc/fstab'
  printf "\n\n"

  if [ "$PART_LABEL" = "gpt" ]
  then
    genfstab -U $CHROOT >> "$CHROOT/etc/fstab"
  else
    genfstab -L $CHROOT >> "$CHROOT/etc/fstab"
  fi

  sed 's/relatime/noatime/g' -i "$CHROOT/etc/fstab"

  return $SUCCESS
}


# setup locale and keymap
setup_locale()
{
  title 'Base System Setup > Locale'

  printl "[+] Setting up $LOCALE locale"
  printf "\n\n"
  sed -i "s/^#en_US.UTF-8/en_US.UTF-8/" "$CHROOT/etc/locale.gen"
  sed -i "s/^#$LOCALE/$LOCALE/" "$CHROOT/etc/locale.gen"
  chroot $CHROOT locale-gen > $verbose 2>&1
  echo "LANG=$LOCALE" > "$CHROOT/etc/locale.conf"
  echo "KEYMAP=$KEYMAP" > "$CHROOT/etc/vconsole.conf"

  return $SUCCESS
}


# setup timezone
setup_time()
{
  if confirm 'Base System Setup > Timezone' '[?] Default: UTC. Choose other timezone [y/n]: '
  then
    for t in $(timedatectl list-timezones)
    do
      echo "    > $t"
    done

    printl "\n[?] What is your (Zone/SubZone): "
    read -r timezone
    chroot $CHROOT ln -sf "/usr/share/zoneinfo/$timezone" /etc/localtime \
      > $verbose 2>&1

    if [ $? -eq 1 ]
    then
      warn 'Do you live on Mars? Setting default time zone...'
      sleep 1
      default_time
    else
      printl "\n[+] Time zone setup correctly\n"
    fi
  else
    printl "\n[+] Setting up default time and timezone\n"
    sleep 2
    default_time
  fi

  printf "\n"

  return $SUCCESS
}


# default time and timezone
default_time()
{
  echo
  warn 'Setting up default time and timezone: UTC'
  printf "\n\n"
  chroot $CHROOT ln -sf /usr/share/zoneinfo/UTC /etc/localtime > $verbose 2>&1

  return $SUCCESS
}


# setup initramfs
setup_initramfs()
{
  title 'Base System Setup > InitramFS'

  printl '[+] Setting up InitramFS'
  printf "\n\n"

  warn 'This can take a while, please wait...'
  printf "\n"
  chroot $CHROOT mkinitcpio -P > $verbose 2>&1

  return $SUCCESS
}


# mount /proc, /sys and /dev
setup_proc_sys_dev()
{
  title 'Base System Setup > Proc Sys Dev'

  printl '[+] Setting up /proc, /sys and /dev'
  printf "\n\n"

  mkdir -p "${CHROOT}/"{proc,sys,dev} > $verbose 2>&1

  mount -t proc proc "$CHROOT/proc" > $verbose 2>&1
  mount --rbind /sys "$CHROOT/sys" > $verbose 2>&1
  mount --make-rslave "$CHROOT/sys" > $verbose 2>&1
  mount --rbind /dev "$CHROOT/dev" > $verbose 2>&1
  mount --make-rslave "$CHROOT/dev" > $verbose 2>&1

  return $SUCCESS
}


# setup hostname
setup_hostname()
{
  title 'Base System Setup > Hostname'

  printl '[+] Setting up hostname'
  printf "\n\n"

  echo "$HOST_NAME" > "$CHROOT/etc/hostname"

  return $SUCCESS
}


##--[[ creating hosts file ]]--##
setup_hosts_file()
{
  sleep_clear 1 && title "Creating hosts file"
  { echo "127.0.0.1       localhost"; echo "::1             localhost";\
    echo "127.0.1.1       $HOST_NAME.localdomain $HOST_NAME"; }\
    > $CHROOT/etc/hosts
      sleep 2
}


# setup boot loader for UEFI/GPT or BIOS/MBR
setup_bootloader()
{
  title 'Base System Setup > Boot Loader'

  if [ "$BOOT_MODE" = 'uefi' ] && [ "$PART_LABEL" = 'gpt' ]
  then
    printl '[+] Setting up EFI boot loader'
    printf "\n\n"

    if [ $DUALBOOT = $TRUE ]; then
      chroot $CHROOT pacman -S grub efibootmgr dosfstools os-prober mtools --noconfirm --overwrite='*' --needed \
        > $verbose 2>&1
    else
      chroot $CHROOT pacman -S grub efibootmgr dosfstools mtools --noconfirm --overwrite='*' --needed \
        > $verbose 2>&1
    fi

    chroot $CHROOT grub-install --target=x86_64-efi --bootloader-id=grub_uefi \
      --recheck > $verbose 2>&1

    chroot $CHROOT grub-mkconfig -o /boot/grub/grub.cfg > $verbose 2>&1

  else
    printl '[+] Setting up GRUB boot loader'
    printf "\n\n"


    if [ $DUALBOOT = $TRUE ]; then
      chroot $CHROOT pacman -S grub dosfstools os-prober mtools --noconfirm --overwrite='*' --needed \
        > $verbose 2>&1
    else
      chroot $CHROOT pacman -S grub dosfstools mtools --noconfirm --overwrite='*' --needed \
        > $verbose 2>&1
    fi

    chroot $CHROOT grub-install --target=i386-pc "$HD_DEV" > $verbose 2>&1
    chroot $CHROOT grub-mkconfig -o /boot/grub/grub.cfg > $verbose 2>&1

  fi

  return $SUCCESS
}


# ask for normal user account to setup
ask_user_account()
{
  if confirm 'Base System Setup > User' '[?] Setup a normal user account [y/n]: '; then
    while [ -z "$NORMAL_USER" ]; do
      printl '[?] User name: '
      read -r NORMAL_USER
    done
  else
    warn "You have to create a user account" && sleep_clear 1 && ask_user_account
  fi

  return $SUCCESS
}


# setup user account, password and environment
setup_user()
{
  user="$(echo "$1" | tr -dc '[:alnum:]_' | tr '[:upper:]' '[:lower:]' |
    cut -c 1-32)"

  title 'Base System Setup > User'

  printl "[+] Setting up $user account"
  printf "\n\n"

  # normal user
  if [ -n "$NORMAL_USER" ]
  then
    chroot $CHROOT groupadd "$user" > $verbose 2>&1
    chroot $CHROOT useradd -g "$user" -d "/home/$user" -s "/bin/bash" \
      -G "$user,wheel,users,video,storage,optical,audio" -m "$user" > $verbose 2>&1
    chroot $CHROOT chown -R "$user":"$user" "/home/$user" > $verbose 2>&1
    printl "[+] Added user: $user"
    printf "\n\n"
  fi

  # password
  res=1337
  printl "[?] Set password for $user: "
  printf "\n\n"
  while [ $res -ne 0 ]
  do
    if [ "$user" = "root" ]
    then
      chroot $CHROOT passwd
    else
      chroot $CHROOT passwd "$user"
    fi
    res=$?
  done

  return $SUCCESS
}

reinitialize_keyring()
{
  title 'Base System Setup > Keyring Reinitialization'

  printl '[+] Reinitializing keyrings'
  printf "\n"
  sleep 2

  chroot $CHROOT pacman -S --overwrite='*' --noconfirm archlinux-keyring \
    > $verbose 2>&1

  return $SUCCESS
}

# install extra (missing) packages
setup_extra_packages()
{
  all=(bluez bluez-hid2hci bluez-tools bluez-utils vim neovim cifs-utils \
    dmraid dosfstools exfat-utils f2fs-tools gpart gptfdisk mtools nilfs-utils \
    ntfs-3g partclone parted partimage amd-ucode intel-ucode networkmanager \
    wpa_supplicant dialog dhcpcd git curl)

  title 'Base System Setup > Extra Packages'

  printl '[+] Installing extra packages'
  printf "\n"

  warn 'This can take a while, please wait...'
  printf "\n"
  sleep 2

  chroot $CHROOT pacman -S --needed --overwrite='*' --noconfirm "${all[@]}" \
    > $verbose 2>&1

  return $SUCCESS
}


# perform system base setup/configurations
setup_base_system()
{
  pass_mirror_conf # copy mirror list to chroot env

  setup_resolvconf
  sleep_clear 1

  install_base_packages
  sleep_clear 1

  setup_resolvconf
  sleep_clear 1

  setup_fstab
  sleep_clear 1

  setup_proc_sys_dev
  sleep_clear 1

  setup_locale
  sleep_clear 1

  # setup_initramfs
  # sleep_clear 1

  setup_hostname
  sleep_clear 1

  setup_hosts_file
  sleep_clear 1

  setup_user "root"
  sleep_clear 1

  ask_user_account
  sleep_clear 1

  if [ -n "$NORMAL_USER" ]; then
    setup_user "$NORMAL_USER"
    sleep_clear 1
  fi

  reinitialize_keyring
  sleep_clear 1
  setup_extra_packages
  sleep_clear 1

  setup_bootloader
  sleep_clear 1

  return $SUCCESS
}


# enable systemd-networkd services
enable_bluetooth_networkd()
{
  title 'Arch Linux Setup > Network'

  printl '[+] Enabling Bluetooth and Networkd'
  printf "\n\n"

  chroot $CHROOT systemctl enable bluetooth NetworkManager > $verbose 2>&1

  return $SUCCESS
}


# ask for archlinux server
ask_mirror_arch()
{
  local mirrold='cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup'

  if confirm 'Pacman Setup > ArchLinux Mirrorlist' \
    "[+] India mirror will be used by default\n\n[?] Look for the best server for your specific [y/n]: "
  then
    printf "\n"
    printl 'This may take time depending on your connection'
    printf "\n"
    $mirrold
    pacman -Sy --noconfirm > $verbose 2>&1
    pacman -S --needed --noconfirm reflector > $verbose 2>&1
    yes | pacman -Scc > $verbose 2>&1
    ask "Name your country: "
    read -r country
    [ -n "$country" ] && reflector --protocol https --verbose --country "$country"\
      -l 10 --sort rate --save /etc/pacman.d/mirrorlist > $verbose 2>&1
    [ -z "$country" ] && reflector --verbose --latest 5 --protocol https --sort rate\
      --save /etc/pacman.d/mirrorlist > $verbose 2>&1
  else
    printf "\n"
    warn 'Using India mirror server'
    $mirrold
    ! command -v curl > $verbose 2>&1 && pacman -Sy curl > $verbose 2>&1
    { curl https://gitlab.com/sum4n/sedo-desktop/-/raw/master/extra/mirrorlist-arch.txt -o\
      /etc/pacman.d/mirrorlist; }
  fi
}


# pass correct config
pass_mirror_conf()
{
  mkdir -p "$CHROOT/etc/pacman.d/" > $verbose 2>&1
  cp -f /etc/pacman.d/mirrorlist "$CHROOT/etc/pacman.d/mirrorlist" \
    > $verbose 2>&1
}

##--[ final step ]--##
##  [[ download the sedo-desktop installer script ]]  ##
final_step()
{
  sleep_clear 1 && title "Final steps to run"
  curl https://gitlab.com/sum4n/sedo-desktop/-/raw/master/sedo_installer -o $CHROOT/home/"$user"/sedo_installer
  chmod +x $CHROOT/home/"$user"/sedo_installer
  chown -R "$user":"$user" "$CHROOT/home/$user/sedo_installer"
  printl "Base arch system is now installed and configured\n" && sleep 1
  printl "After reboot if you want to install [ sedo-desktop ]\n" && sleep 1
  printl "Just run this command:- %s./sedo_installer%s\n" "$YELLOWB" "$NC" && sleep 1
  printl "[+] Finally run those two command after finish the script\n" && sleep 1
  printl "Run:- [%s exit; umount -l /mnt; reboot%s ]\n" "$YELLOWB" "$NC" && sleep 3
  sleep 2
}


# perform sync
sync_disk()
{
  title 'Game Over'

  printl '[+] Syncing disk'
  printf "\n\n"

  sync

  return $SUCCESS
}


###----[[[ main flow controller of the script ]]]----###
main()
{
  # do some ENV checks
  sleep_clear 0
  check_uid
  check_env
  check_boot_mode

  # output mode
  ask_output_mode
  sleep_clear 0

  # locale
  ask_locale
  set_locale
  sleep_clear 0

  # keymap
  ask_keymap
  set_keymap
  sleep_clear 0

  # network
  ask_hostname
  sleep_clear 0

  get_net_ifs
  ask_net_conf_mode
  if [ "$NET_CONF_MODE" != "$NET_CONF_SKIP" ]
  then
    ask_net_if
  fi
  case "$NET_CONF_MODE" in
    "$NET_CONF_AUTO")
      net_conf_auto
      ;;
    "$NET_CONF_WLAN")
      ask_wlan_data
      net_conf_wlan
      ;;
    "$NET_CONF_MANUAL")
      ask_net_addr
      net_conf_manual
      ;;
    "$NET_CONF_SKIP")
      ;;
    *)
      ;;
  esac
  sleep_clear 1
  check_inet_conn
  sleep_clear 1

  # pacman
  ask_mirror_arch
  paralleldownloads
  sleep_clear 1
  update_pacman

  # Update keyrings
  install_keyrings

  # hard drive
  sleep_clear 1
  get_hd_devs
  sleep_clear 1
  ask_hd_dev
  sleep_clear 1
  ask_dualboot
  sleep_clear 1
  umount_filesystems 'harddrive'
  sleep_clear 1
  ask_cfdisk
  sleep_clear 3
  get_partition_label
  sleep_clear 1
  ask_partitions
  sleep_clear 1
  print_partitions
  sleep_clear 1
  ask_formatting
  clear
  make_partitions
  clear
  mount_filesystems
  sleep_clear 1

  # arch linux
  setup_base_system
  sleep_clear 1
  setup_time
  sleep_clear 1

  enable_bluetooth_networkd

  # epilog
  umount_filesystems
  sleep_clear 1
  final_setup
  sleep_clear 1
  sync_disk

  return $SUCCESS
}


###----[[[ start here ]]]----###
main "$@"

###----[[[ E O F ]]]----###

