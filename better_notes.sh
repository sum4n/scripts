#!/bin/env bash

# =================================================== ::
#
# Author       ::sum4n
# Source       ::https://gitlab.com/sum4n/bin
# Maintainer   ::to_suman@outlook.com
# Project      ::
#
# =================================================== ::

## --- START --- ##

## --- check figlet is installed in system --- ##
#
while true; do
	if ! command -v figlet &>/dev/null; then
		if command -v pacman &>/dev/null; then
			sudo pacman -S figlet
			break
		elif command -v apt &>/dev/null; then
			sudo apt install figlet
			break
		else
			print "Please install figlet with your package-manager!!"
			exit
		fi
	fi
	break
done
clear

## --- check exa is installed or not --- ##
while true; do
	if ! command -v exa &>/dev/null; then
		if command -v pacman &>/dev/null; then
			sudo pacman -S exa
			break
		elif command -v apt &>/dev/null; then
			sudo apt install exa
			break
		else
			echo -e "Please install exa with your package manager!!"
			exit
		fi
	fi
	break
done

## --- make sure user have note directory --- ##
#
if test ! -d "$HOME/notes_todo"; then # -- todo folder
	mkdir "$HOME/notes_todo"
fi
if test ! -d "$HOME/notes_todo/notes"; then # -- notes folder
	mkdir "$HOME/notes_todo/notes"
fi

## --- BANNER ---##
#
figlet -s -f digital -c "A Simple Terminal Note Taking Script"
printf "\n "
printf "                  created by: Suman Kumar Das (sum4n)\n"
printf "\n "

## --- File Variable --- ##
#
TODO="$HOME/notes_todo"
NOTES="$HOME/notes_todo/notes"
DATE="$(date | awk '{print $2 $3"-"$6}')"
# SCHEDULE_DATE="$(date --date="2 days" | awk '{print $2 $3"-"$6}')"
# my_term="alacritty"
my_editor="nvim"
view_todo="$(exa -maG --color=always --icons "$TODO" | tail -n10 | paste -sd' ')"
view_notes="$(exa -maG --color=always --icons "$NOTES" | tail -n10 | paste -sd' ')"

## --- selection for todo or notes --- ##
#
while true; do
	echo -e "\nYou want to Create or View A?"
	read -rp "Options: [1]-ToDo [2]-Note [3]-Exit! " reply

	## -- todo section start -- ##

	if [ "$reply" = "1" ]; then
		## --- add append or view todo --- ##
		while true; do
			echo -e "\nYou selected ToDo!\nWhat you want?"
			read -rp "Options: [1]-Add_ToDo [2]-View_ToDo [3]-Exit_From! " todo

			## --- add, append or view task --- ##

			if [ "$todo" = "1" ]; then

				echo -e "\nYou selected Add a ToDo!"
				read -rp "Select a date - (Format = Oct14) : " date
				read -rp "Select a time - (Format = HH:MM) : " time
				read -rp "Do a entry : " entry

				## --- check if file already exist then add or append a task --- ##

				if [ -f "$TODO/$date.md" ]; then
					printf "\n### %s %s :\n# %s\n" "$date" "$time" "$entry" >>"$TODO/$date.md"
					cd "$TODO" && $my_editor "$TODO/$date.md"
					break
				else
					touch "$TODO/$date.md"
					figlet -s -f digital -c "$date" >>"$TODO/$date.md"
					printf "\n### %s %s :\n# %s\n" "$date" "$time" "$entry" >>"$TODO/$date.md"
					cd "$TODO" && $my_editor "$TODO/$date.md"
					break
				fi

			## --- view to-do --- ##

			elif [ "$todo" = "2" ]; then
				clear
				echo -e "\nYou selected View a ToDo!\nSo search a todo by Datewise or Edit-all?"
				read -rp "Options: [1]-View_All [2]-View_By_Date " todo
				while true; do
					if [ "$todo" = "1" ]; then
						cd "$TODO" && $my_editor "$TODO/"
						break
					else
						clear
						echo -e "\nHere is your last 10 edited ToDo's. Choose from here or do your search"
						echo -e "\n[ $view_todo ]\n"
						read -rp "Which date would you like to see? Date - (Format = Oct14) : " day
						cd "$TODO" && test -f "$TODO/$day.md" && $my_editor "$TODO/$day.md" || echo -e "No schedule In this day so chill"
						sleep 2
						clear
						break
					fi
				done
				break
			elif [ "$todo" = "3" ]; then
				clear
				echo -e "Ok exited!"
				break
			else
				echo -e "Please: select the wrigt option!\n"
				sleep 1
			fi
		done

	## --- Note section start --- ##

	elif [ "$reply" = "2" ]; then
		## --- add, append or view a Note --- ##
		while true; do

			echo -e "\nYou selected Notes\nWhat you want?"
			read -rp "Options: [1]-Add-Note [2]-View-Note [3]-Exit-From! " notes

			## --- add or append note --- ##
			if [ "$notes" = "1" ]; then
				read -rp "Pick current time for this note - (Format = HH:MM) : " time
				read -rp "Enter a tagline for header of this note : " entry

				## --- check the selected file is already exist --- ##

				if [ -f "$NOTES/note_$DATE.md" ]; then
					printf "\n# %s - %s\n" "$entry" "$time" >>"$NOTES/note_$DATE.md"
					cd "$NOTES" && $my_editor "$NOTES/note_$DATE.md"
					break
				else
					touch "$NOTES/note_$DATE.md"
					figlet -s -f digital -c "$DATE" >>"$NOTES/note_$DATE.md"
					printf "\n# %s - %s\n" "$entry" "$time" >>"$NOTES/note_$DATE.md"
					cd "$NOTES" && $my_editor "$NOTES/note_$DATE.md"
					break
				fi

			## --- view section in note --- ##

			elif [ "$notes" = "2" ]; then
				## --- view notes --- ##

				echo -e "\nYou selected view-notes"
				echo -e "You want to search by Datewise or see All in a editor?"
				read -rp "Options: [1]-View-All [2]-View-By-Date [3]-Exit-From! " note_view
				while true; do
					if [ "$note_view" = "1" ]; then
						cd "$NOTES" && $my_editor
						break
					elif [ "$note_view" = "2" ]; then

						clear
						echo -e "\nHere is your last 10 edited files. Choose from here or do your search"
						echo -e "\n[ $view_notes ]\n"
						read -rp "Which date note you like to see (Format = Oct14)? " day
						read -rp "Which year for this note (Format - YYYY)? " year
						cd "$NOTES" && test -f "note_$day-$year.md" && $my_editor "note_$day-$year.md" || echo -e "You dont've any notes on this date\n"
						sleep 2
						clear
						break

					elif [ "$note_view" = "3" ]; then
						clear
						echo -e "Ok exited!\n"
						break
					else
						echo -e "Please: select the current options!"
						sleep 1
					fi
				done
			elif [ "$notes" = "3" ]; then
				clear
				echo -e "Ok exited!"
				break
			else
				echo -e "Please: select the wrigt option!\n"
				sleep 1
			fi
		done
	elif [ "$reply" = "3" ]; then
		clear
		echo -e "\nTerminated the Script!!\n"
		exit
	else
		echo -e "Please: select a wright option!!"
		sleep 1
	fi
done
