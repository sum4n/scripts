#!/bin/env bash

# Author            - sum4n
# Source            - https://gitlab.com/sum4n
# Mail              - to_suman@outlook.com
# Created           - 2023-06-13
# About             - help to find keys


awk '/^[a-z]/ && last {print "<small>",$0,"\t",last,"</small>"} {last=""} /^#/{last=$0}' ~/.config/bspwm/sxhkdrc | \
column -t -s $'\t' | rofi -dmenu -i -markup-rows -no-show-icons -width 1000 -lines 15 -yoffset 40
