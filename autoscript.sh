#!/bin/env bash

# =================================================== ::
#
# Author       ::sum4n
# Source       ::https://gitlab.com/sum4n/SEDO
# Maintainer   ::to_suman@outlook.com
# Project      ::Auto create script in bin folder
#
# =================================================== ::

## --- start --- ##
#
_rofi="rofi -theme ~/.config/rofi/themes/min-search.rasi"
BIN="$HOME/bin" #-- default bin folder
my_term="kitty"
my_editor="nvim"

if test ! -d "$BIN"; then #-- check if folder exist
	mkdir "$BIN"
fi

option=$(ls "$BIN" | $_rofi -dmenu -p "add/edit") #-- variable for create or edit a script

for script in $option; do #-- check if already a file exist or create a new file
	if test -f "$BIN/$script"; then
		cd "$BIN" && "$my_term" -e "$my_editor" "$script"
		break
	else
		cd "$BIN" && touch "$script".sh && chmod +x "$script".sh && printf "#!/usr/bin/env bash\n\n\
#Author        :: sum4n\n\
#Source        :: https://gitlab.com/sum4n/bin\n\
#Maintainer    :: to_suman@outlook.com\n\
#Project       :: \n\n#--- start ---#\n\n" >"$script".sh \
      && "$my_term" -e "$my_editor" "$script".sh
		break
	fi
	break
done

## --- end of script --- ##
