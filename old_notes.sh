#!/bin/env bash

# =================================================== ::
#
# Author       ::sum4n
# Source       ::https://gitlab.com/sum4n/bin
# Maintainer   ::to_suman@outlook.com
# Project      ::Note taker && Task scheduler
#
# =================================================== ::


## --- check figlet is installed in system --- ##
#
while true; do
  if ! command -v figlet &> /dev/null; then
    if command -v pacman &> /dev/null; then
      sudo pacman -S figlet
      break
    elif command -v apt &> /dev/null; then
      sudo apt install figlet
      break
    else
      print "Please install figlet with your package-manager!!"
      exit
    fi
  fi; break
done; clear

## --- make sure user have note directory --- ##
#
if test ! -d "$HOME/notes_todo"; then # -- todo folder
  mkdir "$HOME/notes_todo"
fi
if test ! -d "$HOME/notes_todo/notes"; then # -- notes folder
  mkdir "$HOME/notes_todo/notes"
fi

## --- BANNER ---##
#
figlet -s -f digital "A Simple Terminal Note Taking Script"
printf "\n "
printf "                  created by: Suman Kumar Das (sum4n)\n"
printf "\n "

## --- File Variable --- ##
#
TODO="$HOME/notes_todo"
NOTES="$HOME/notes_todo/notes"
DATE="$(date | awk '{print $2 $3"-"$6}')"
SCHEDULE_DATE="$(date --date="2 days" | awk '{print $2 $3"-"$6}')"
my_term="alacritty"
my_editor="nvim"

## --- selection for todo or notes
#
echo -e "You want to Take A Note or Schedule A Task?"
option="Todo Note No!"
select reply in $option; do


  ### --- To-do section start --- ##
 

  if [ "$reply" = "Todo" ]; then
    echo -e "Make a selection"
    option="Add View Exit!"
    select reply in $option; do

      ## --- add, append or view a Scheduled-task --- ##

      if [ "$reply" = "Add" ]; then

        read -rp "Select a date - (Format = Oct14) : " date
        read -rp "Select a time - (Format = HH:MM AM/PM) : " time
        read -rp "Do a entry : " entry
        #
        ## --- check if file already exist --- ##
        #
        if [ -f "$TODO/$date.md" ]; then
          printf "%s %s :\n %s\n" "$date" "$time" "$entry" >> "$TODO/$date.md"
          cd "$TODO" && $my_term -e $my_editor "$TODO/$date.md"
          break
        else
          touch "$TODO/$date.md"
          figlet -s -f digital "$date" >> "$TODO/$date.md"
          printf "%s %s :\n %s\n" "$date" "$time" "$entry" >> "$TODO/$date.md"
          cd "$TODO" && $my_term -e $my_editor "$TODO/$date.md"
          break
        fi
        break

      ## --- view to-do --- ##

      elif [ "$reply" = "View" ]; then
        echo -e "\nMake a selection"
        view="Date All"
        select do_view in $view; do
          if [ "$do_view" = "All" ]; then
            cd "$TODO" && $my_term -e $my_editor "$TODO/"
            break
          else
            echo -e "Which date would you like to see?\n"
            read -rp "Date - (Format = Oct14) : " day
            cd "$TODO" && test -f "$TODO/$day.md" && $my_term -e $my_editor "$TODO/$day.md" || echo -e "No schedule In this day so chill\n"
            break
          fi
        done
        break
      elif [ "$reply" = "Exit!" ]; then
        exit
      else
        echo -e "Please: select the wrigt option!\n"
        sleep 1
      fi
    done
    break


  ## --- Note section start --- ##


  elif [ "$reply" = "Note" ]; then

    ## --- add, append or view a Note --- ##

    echo -e "Make a selection!"
    option="Add View Exit!"
    select reply in $option; do

      ## --- add note section --- ##

      if [ "$reply" = "Add" ]; then
        read -rp "Pick current time for this note - (Format = HH:MM) : " time
        read -rp "Enter a tagline for header of this note : " entry

        ## --- check the selected file is already exist --- ##

        if [ -f "$NOTES/note_$DATE.md" ]; then
          printf "|| [ %s ] - %s ||\n" "$entry" "$time" >> "$NOTES/note_$DATE.md"
          cd "$NOTES" && $my_term -e $my_editor "$NOTES/note_$DATE.md"
          break
        else
          touch "$NOTES/note_$DATE.md"
          figlet -s -f digital "$DATE" >> "$NOTES/note_$DATE.md"
          printf "|| [ %s ] - %s ||\n" "$entry" "$time" >> "$NOTES/note_$DATE.md"
          cd "$NOTES" && $my_term -e $my_editor "$NOTES/note_$DATE.md"
          break
        fi

        ## --- view section in note --- ##

      elif [ "$reply" = "View" ]; then
        echo -e "Would you like to see datewise or all?"
        note_opt="Date All Exit!"
        select note_view in $note_opt; do
          if [ "$note_view" = "All" ]; then
            cd "$NOTES" && $my_term -e $my_editor
            break
          elif [ "$note_view" = "Date" ]; then
            read -rp "Which date note you like to see (Format = Oct14)? " day
            read -rp "Which year for this note (Format - YYYY)? " year
            cd "$NOTES" && test -f "note_$day-$year.md" && $my_term -e $my_editor "note_$day-$year.md" || echo -e "You dont've any notes on this date\nYou like to search the notes by (1)Date (2)All (3)Exit!"
          elif [ "$note_view" = "Exit!" ]; then
            break
          else
            echo -e "Please: select the current options!"
            sleep 1
          fi
        done
      elif [ "$reply" = "Exit!" ]; then
        break
      else
        echo -e "Please: select the wrigt option!\n"
        sleep 1
      fi
    done
    break
  elif [ "$reply" = "No!" ]; then
    echo -e "Terminated the task!!\n"
    exit
  else
    echo -e "Please: select a wright option!!\n"
    sleep 1
  fi
done

echo hello!!










