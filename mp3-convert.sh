#!/bin/env bash

# =================================================== ::
#
# Author       ::sum4n
# Source       ::https://gitlab.com/sum4n/bin
# Maintainer   ::to_suman@outlook.com
# Project      ::mp3 converter
#
# =================================================== ::

#: dependeci - ffmpeg

# [ colors ] #
greenb="$(tput bold ; tput setaf 2)"
yellowb="$(tput bold setaf 3)"
blueb="$(tput bold setaf 4)"
cyanb="$(tput bold setaf 6)"
redb="$(tput bold setaf 1)"
noc="$(tput sgr0)"

# [ variables ] #
name=''
to_name=''

# [ functions ] #

printn()
{
  fmt="${1}"

  shift
  printf "\n%s$fmt%s" "$cyanb" "$@" "$noc"

}

# -- [ print formatted output ] -- #
nor()
{
  gen="${1}"

  shift
  printf "%s$gen%s" "$blueb" "$@" "$noc"

}

# --- [ print error and return failure ] --- #
warn()
{
  printf "%s[!] WARNING: %s%s\n" "$redb" "$@" "$noc"

}

#----[ check dependeci available or not ]----#
available()
{
  ! command -v ffmpeg &>/dev/null && warn "[ffmpeg] is not installed" && sleep 1 \
    && clear && printf "%sInstalling [ffmpeg] in system..%s\n" "$greenb" "$noc" && sleep 1 \
    && if command -v pacman &>/dev/null; then sudo pacman -S ffmpeg; fi \
    && if command -v apt &>/dev/null; then sudo apt install ffmpeg; fi
}

#----[ ask for file name or path of the file ]----#
ask_file()
{
  while [ -z "$name" ]; do
    printn "[?] Name of the file or path of the file: "
    read -r name
    [ -z "$name" ] && { warn "specify the file name"; sleep 1; clear; }
    quit
  done
}

ask_cv_file()
{
  while [ -z "$to_name" ]; do
    printf "\n%sMain file is: %s%s\n" "$yellowb" "$name" "$noc"
    nor "\n[?] Name of converted file with extension\n   and with or without path: "
    read -r to_name
    [ -z "$to_name" ] && { warn "specify the converted file name"; sleep 1; clear; }
    [ -n "$to_name" ] && { clear; printf "\n%sMain file is: %s%s\n" "$yellowb" "$name" "$noc";\
        printf "%sNew converted file is: %s%s\n" "$greenb" "$name" "$noc"; sleep 4; }
    quit
  done
}

#----[ quit the script ]----#
quit()
{
  if [ "$to_name" = "q" ] || [ "$name" = "q" ]; then
    warn "terminate the script.." && sleep 1
    exit 66
  fi
}

#----[ converter ]----#
converter()
{
  ffmpeg -i "${name[*]}" -vn "${to_name[*]}"
}


#----[ main function flow ]----#
main()
{
  clear
  available
  ! command -v ffmpeg &>/dev/null && { printf "%sPlease install [ffmpeg] package using\
  your package manager%s\n" "$redb" "$noc"; sleep 1; exit 55; }
  clear
  ask_file
  sleep 1 && clear
  ask_cv_file
  sleep 1
  converter
}

#----[ start ]----#
main "$@"

#----[[ E O F ]]----#


