#!/bin/env bash

# =================================================== ::
#
# Author       ::sum4n
# Source       ::https://gitlab.com/sum4n/bin
# Maintainer   ::to_suman@outlook.com
# Project      ::See full forecast in terminal
#
# =================================================== ::

## --- See the full forecast in terminal --- ##
read -rp "What is your City: " city
city_improved=$(echo "$city" | awk -F " " '{print $1 $2}')

## --- Echo the output --- ##
if [ -z "$city_improved" ]; then curl -s "wttr.in/$1?"; else curl -s wttr.in/"$city_improved"; fi
